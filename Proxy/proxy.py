import zmq
import os

SERVERS_PORT = '50000'
CLIENTS_PORT = '50010'

def s2b(string_data):
    '''
    Funcion para convertir datos tipo string a tipo bytes
    '''
    return string_data.encode('ascii')

def b2s(bytes_data):
    '''
    Funcion para convertir datos tipo bytes a tipo string
    '''
    return bytes_data.decode('ascii')

class Proxy:

    def __init__(self):
        
        # Inicializa el contexto
        self.context = zmq.Context()

        # Crea los sockets para responder solicitudes de servidores y clientes
        self.servers = self.context.socket(zmq.REP)
        self.clients = self.context.socket(zmq.REP)

        # Empieza a escuchar a servidores y clientes por los puertos indicados
        self.servers.bind('tcp://*:{}'.format(SERVERS_PORT))
        self.clients.bind('tcp://*:{}'.format(CLIENTS_PORT))

        # Crea el Poller para vigilar la actividad de cada socket
        self.poller = zmq.Poller()
        self.poller.register(self.servers, zmq.POLLIN)
        self.poller.register(self.clients, zmq.POLLIN)

        # Lista con las direcciones de los servidores
        self.serversAddresses = []

        # Diccionario donde Key = Sha1 y Value = Servidor que tiene ese Sha1
        self.sha1Table = {}

        # Diccionario donde Key = Usuario y Value = Diccionario de cada archivo que tiene el usuario
        self.usersTable = {}

        # Contador de servidores
        self.serversCounter = 0


    def run(self):
        '''
        Procedimiento para correr el servidor
        '''

        os.system('clear')
        print("   --- FILE PROXY ---")
        print(" STATUS -> Running...")

        # Ciclo infinito para resolver solicitudes de clientes y servidores
        while True:
            event = dict(self.poller.poll())

            # Hay actividad en el socket de servidores
            if self.servers in event:

                request, *data = self.servers.recv_multipart()
                print(request)

                if request == b'NEW_SERVER':

                    serverAddress = data[0].decode('ascii')
                    self.serversAddresses.append(serverAddress)

                    self.servers.send(s2b(str(self.serversCounter)))

                    self.serversCounter += 1


            # Hay actividad en el socket de clientes
            elif self.clients in event:

                request, *data = self.clients.recv_multipart()
                print(request)
                
                # Solicitud de obtener las direcciones de los servidores
                if request == b'SERVERS_ADDRESSES':

                    serversAddresses = str(self.serversAddresses).encode('ascii')
                    self.clients.send(serversAddresses)

                # Solicitud para recibir la informacion de la subida de un archivo
                elif request == b'UPLOAD_FINISHED':

                    userName = b2s(data[0])
                    fileName = b2s(data[1])
                    segmentsInformation = eval(b2s(data[2]))
                    indexInformation = eval(b2s(data[3]))
                    
                    self.usersTable[userName][fileName] = ['OWNER', indexInformation[0]]

                    for sha1DataSegment, serverAddress in segmentsInformation.items():

                        self.sha1Table[sha1DataSegment] = serverAddress

                    self.sha1Table[indexInformation[0]] = indexInformation[1]

                    self.clients.send(b'DONE')

                # Solicitud para listar los archivos de un usuario
                elif request == b'LIST_FILES':

                    userName = b2s(data[0])
                    aux = []
                    for fileName, fileInformation in self.usersTable[userName].items():

                        aux.append([fileName, fileInformation[0]])


                    self.clients.send(s2b(str(aux)))

                # Solicitud para crear un usuario
                elif request == b'NEW_USER':

                    userName = b2s(data[0])
                    self.usersTable[userName] = {}
                    self.clients.send(b'DONE')

                # Solicitud para verificar si un usuario esta libre o ya existe
                elif request == b'CHECK_USER':

                    userName = b2s(data[0])

                    if not userName in self.usersTable:
                        self.clients.send(b'FREE_USER')
                    else:
                        self.clients.send(b'TAKEN_USER')

                # Solicitud para compartir un archivo con otro usuario
                elif request == b'SHARE_FILE':

                    ownerName = b2s(data[0])
                    shareWith = b2s(data[1])
                    fileName = b2s(data[2])

                    try:
                        indexInformation = self.usersTable[ownerName][fileName]
                        self.usersTable[shareWith][fileName] = ['SHARED', indexInformation[1]]
                        response = b'DONE'
                        
                    except:
                        response = b'ERROR_FILE'

                    

                    self.clients.send(response)

                elif request == b'SHA1_INDEX':

                    userName = b2s(data[0])
                    fileName = b2s(data[1])
                    try:
                        sha1Index = self.usersTable[userName][fileName][1]
                    except:
                        sha1Index = 'None'

                    self.clients.send(s2b(sha1Index))

                elif request == b'SHA1_ADDRESSES':

                    sha1List = eval(b2s(data[0]))
                    sha1Addresses = []
                    for sha1 in sha1List:
                        sha1Addresses.append(self.sha1Table[sha1])

                    self.clients.send(s2b(str(sha1Addresses)))


def main():
    proxy = Proxy()
    try:
        proxy.run()
    except:
        print(" STATUS -> Ended")



if __name__ == '__main__':
    
    main()