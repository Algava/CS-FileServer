import zmq
import os
import shutil

PROXY_IP = 'localhost'
PROXY_PORT = '50000'

CLIENT_IP = 'localhost'


def s2b(string_data):
    '''
    Funcion para convertir datos tipo string a tipo bytes
    '''
    return string_data.encode('ascii')

def b2s(bytes_data):
    '''
    Funcion para convertir datos tipo bytes a tipo string
    '''
    return bytes_data.decode('ascii')

class Server:

    def __init__(self):
        
        # Inicializa el contexto
        self.context = zmq.Context()

        # Crea sockets para hacer solicitudes al proxy, y socket para responder a clientes
        self.proxy = self.context.socket(zmq.REQ)
        self.clients = self.context.socket(zmq.REP)

        # Conecta con el Proxy
        self.proxy.connect('tcp://{}:{}'.format(PROXY_IP, PROXY_PORT))

        # En el socket de clientes, se coloca a escuchar por un puerto random entre 40000 y 49999
        # Y guarda el puerto en self.clientsPort
        self.clientsPort = self.clients.bind_to_random_port('tcp://*', min_port = 40000, max_port = 49990, max_tries = 100)


    def run(self):
        '''
        Procedimiento para correr el servidor
        '''

        os.system('clear')
        print("   --- FILE SERVER ---")
        print(" STATUS -> Running...")

        # Ciclo infinito para escuchar peticiones de los clientes
        while True:

            request, *data = self.clients.recv_multipart()
            print(request)

            # Procedimiento si el cliente esta solicitando subir un archivo
            if request == b'UPLOAD_FILE':

                # Obtiene el nombre del archivo, datos del archivo y la ruta del archivo
                fileName = b2s(data[0])
                fileData = data[1]
                filePath = self.folderPath + fileName

                # Crea el archivo de escritura en bytes y guarda los datos del archivo recibido
                with open(filePath, 'wb') as file:
                    file.write(fileData)

                # Confirma al cliente que realizo la solicitud
                self.clients.send(b'DONE')

            # Procedimiento si el cliente esta solicitando bajar un archivo
            elif request == b'DOWNLOAD_FILE':

                fileName = b2s(data[0])
                filePath = self.folderPath + fileName

                with open(filePath, 'rb') as file:
                    fileData = file.read()

                self.clients.send(fileData)


    def initialize(self):
        '''
        Procedimiento para inicializar el servidor
        '''
        # Envia solicitud al proxy de que es un nuevo servidor, con la IP y PUERTO para escuchar clientes
        self.proxy.send_multipart([b'NEW_SERVER', '{}:{}'.format(CLIENT_IP, self.clientsPort).encode('ascii')])
        
        # Recive el numero del servidor que le pasa el proxy
        self.serverNumber = b2s(self.proxy.recv())

        # Guarda la ruta donde guardara los archivos
        self.folderPath = '{}/Server_{}/'.format(os.path.dirname(os.path.abspath(__file__)), self.serverNumber)
        
        # Si la ruta donde guardara los archivos ya existe, elimina la carpeta
        if os.path.exists(self.folderPath):
            os.rmdir(self.folderPath)

        # Crea la ruta donde guardara los archivos
        os.mkdir(self.folderPath)


def main():
    server = Server()
    try:
        server.initialize()
        server.run()
    except:
        print(" STATUS -> Ended")
        shutil.rmtree(server.folderPath)



if __name__ == '__main__':
    
    main()